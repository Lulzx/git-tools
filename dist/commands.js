"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var search = {
  regex: /\/search\s(\w+)\s(.+)/,
  inlineRegex: /(\w+)\s(.+)/,
  help: ""
};

var help = {
  regex: /\/help/,
  initMessage: "I can search *users*, *repos* and *issues* for you.\n\n*users*:\n  /search u user\n  /search u google\n\n*repos*:\n  /search r repo\n  /search r react-github\n\n*issues*:\n  /search i issue\n  /search i jquery\n\nNote that you can use _user_, _users_, _repo_, _repos_, " + "_issue_ or _issues_ instead of that _u_, _r_, or _i_.\nThose are just some simple shortcuts.",
  help: ""
};

var start = {
  regex: /\/start/,
  initMessage: "Heyo! I'm here to help you in order to find *repositories*, *users* and *issues* from *GitHub* directly in *Telegram* \nSend /help to know how to use me."
};

var commands = {
  search: search,
  help: help,
  start: start
};

exports.default = commands;
module.exports = exports['default'];
//# sourceMappingURL=commands.js.map
